#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 22:20:36 2018

@author: voltar
"""

from astropy.io import fits
import numpy as np
import sep


# import fitsio
from ImageHandler import ImageHandler


class ImageData(object):
    def __init__(self, fitsFileName):
        super(ImageData, self).__init__()
        print("load: " + fitsFileName)
        hdulist = fits.open(fitsFileName)
        self.data = hdulist[0].data

        self.data = self.data[::-1]  # reverse order of array such that the image is displayed correctly

        # we need to bring the array into a C Contiguous convention, I
        # don't know what this is but this two lines make source extractor work.
        self.data = np.ascontiguousarray(self.data)
        self.data = self.data.byteswap().newbyteorder()
        imagehandler = ImageHandler()
        imagehandler.setImage(self.data)

    def binData(self, binSize):
        print("not yet implemented")

    def generateGrayscaleImage(self):
        # need to subtract background, else there are too many detections
        self.data_sub = self.data# - self.bkg

        # ugly gray scaling method
        self.clippedData = np.clip(a=self.data_sub, a_min=0, a_max=0.1)
        self.clippedData = self.clippedData * 2550
        self.clippedData = np.clip(a=self.clippedData, a_min=0, a_max=255)
        self.clippedData = np.uint8(self.clippedData)
        imagehandler = ImageHandler()
        imagehandler.setCurrentImage(self.clippedData)
        return self.clippedData  # data needs to come as uint between 0 ... 255 (grayscales)
        # print(data[20,20])

