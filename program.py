#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QGraphicsScene

import SubvanDerBurg2016Dialog
from ImageData import ImageData
from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
from PyQt5.QtGui import QImage
from Parameters import Parameters as para

import SubDialogSexConfig
import SubGalaxyWindow

# https://stackoverflow.com/questions/34232632/convert-python-opencv-image-numpy-array-to-pyqt-qpixmap-image
def cv2Qt(cvImg):
    height, width, channel = cvImg.shape
    bytesPerLine = 3 * width
    qImg = QImage(cvImg.data, width, height, bytesPerLine, QImage.Format_RGB888)
    return qImg

def showSexConfigDialogWindow():
    dialog = QtWidgets.QDialog()
    ui_d = SubDialogSexConfig.SubDialogSexConfig()
    ui_d.setupUi(dialog)
    ui_d.buttonBox.accepted.connect(ui_d.accept)
    ui_d.lineEdit_Threshold.setText(str(para.sex_threshold))
    ui_d.lineEdit_MinPixel.setText(str(para.sex_minPixel))
    ui_d.lineEdit_MaxPixel.setText(str(para.sex_maxPixel))
    ui_d.lineEdit_MaskThreshold.setText(str(para.sex_maskThreshold))
    print("should show dialog")
    dialog.setWindowModality(QtCore.Qt.ApplicationModal)
    dialog.exec_()

def showvanDerBurg2016DialogWindow(graphicsView):
    dialog = QtWidgets.QDialog()
    ui_d = SubvanDerBurg2016Dialog.SubvanDerBurg2016Dialog()
    ui_d.setupUi(dialog)
    ui_d.pushButton_Run.clicked.connect(lambda: ui_d.run(graphicsView))
    ui_d.lineEdit_Threshold.setText(str(para.sex_threshold))
    ui_d.lineEdit_MinPixel.setText(str(para.sex_minPixel))
    ui_d.lineEdit_MaxPixel.setText(str(para.sex_maxPixel))
    print("should show vanDerBurg2016 dialog")
    #dialog.setWindowModality(QtCore.Qt.ApplicationModal)
    dialog.exec_()


if __name__ == "__main__":
    print("is Main")

    # check that only 1 instance of app lives.
    # https://stackoverflow.com/questions/10888045/simple-ipython-example-raises-exception-on-sys-exit
    app=QtWidgets.QApplication.instance() # checks if QApplication already exists 
    if not app: # create QApplication if it doesnt exist 
        app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)

    #make and display main window
    MainWindow = QtWidgets.QMainWindow()
    ui = SubGalaxyWindow.SubGalaxyWindow()
    ui.setupUi(MainWindow)
    ui.setupActions()
    ui.actionParameters.triggered.connect(lambda: showSexConfigDialogWindow()) # TODO: should be moved into SubGalaxyWindow
    ui.actionvan_der_Burg_16.triggered.connect(lambda: showvanDerBurg2016DialogWindow(ui.graphicsView)) # TODO: should be moved into SubGalaxyWindow

    #scene = QGraphicsScene()



    
    MainWindow.show()
    MainWindow.close
    app.exec_()



