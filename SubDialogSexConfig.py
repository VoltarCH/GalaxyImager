#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 10:06:13 2018

@author: voltar
"""
from PyQt5 import QtWidgets
from Parameters import Parameters
from Ui_DialogSexConfig import Ui_DialogSexConfig


class SubDialogSexConfig(Ui_DialogSexConfig):
    def __init__(self):
        super(SubDialogSexConfig, self).__init__()

    def accept(self):
        print("accept")
        Parameters.sex_threshold = self.lineEdit_Threshold.text()
        Parameters.sex_maxPixel = self.lineEdit_MaxPixel.text()
        Parameters.sex_minPixel = self.lineEdit_MinPixel.text()
        Parameters.sex_maskThreshold = self.lineEdit_MaskThreshold.text()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication.instance()  # checks if QApplication already exists
    if not app:  # create QApplication if it doesnt exist
        app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    Dialog = QtWidgets.QDialog()
    ui = SubDialogSexConfig()
    ui.setupUi(Dialog)
    ui.buttonBox.accepted.connect(ui.accept)
    Dialog.show()
    sys.exit(app.exec_())
