#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Jun 14 2018

@author: scintilla
"""

import numpy as np
import cv2



class Filter():

    defaultKSize=(2,2)
    defaultAnchor=(-1,1)
    defaultBorderType='BORDER_DEFAULT';

    def __init__(self, ksize=defaultKSize,anchor=defaultAnchor,borderType=defaultBorderType):
        self.ksize=ksize
        self.anchor=anchor
        self.borderType=borderType


    def applyFilter(self,img):
        filteredImg=cv2.blur(img, self.ksize,self.anchor,self.borderType)
        return filteredImg;