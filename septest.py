#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 08:50:47 2018
https://sep.readthedocs.io/en/v1.0.x/tutorial.html
@author: voltar
"""

import numpy as np
import sep
import fitsio
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

data = fitsio.read("J024104.00-081520.0.fits")

# show the image
plt.figure(1)
m, s = np.mean(data), np.std(data)
plt.imshow(data, interpolation='nearest', cmap='gray', vmin=m-s, vmax=m+s, origin='lower')
plt.colorbar();

bkg = sep.Background(data)
print(bkg.globalback)
print(bkg.globalrms)

# evaluate background as 2-d array, same size as original image
bkg_image = bkg.back()
# bkg_image = np.array(bkg) # equivalent to above

plt.figure(2)
# show the background
plt.imshow(bkg_image, interpolation='nearest', cmap='gray', origin='lower')
plt.colorbar();

# evaluate the background noise as 2-d array, same size as original image
bkg_rms = bkg.rms()

plt.figure(3)
# show the background noise
plt.imshow(bkg_rms, interpolation='nearest', cmap='gray', origin='lower')
plt.colorbar();

# subtract the background
data_sub = data - bkg

objects = sep.extract(data_sub,thresh= 0.5, err=bkg.globalrms, minarea=2000,maskthresh=0.3)

# how many objects were detected
len(objects)



# plot background-subtracted image
fig, ax = plt.subplots()
m, s = np.mean(data_sub), np.std(data_sub)
im = ax.imshow(data_sub, interpolation='nearest', cmap='gray',
               vmin=m-s, vmax=m+s, origin='lower')

# plot an ellipse for each object
for i in range(len(objects)):
    e = Ellipse(xy=(objects['x'][i], objects['y'][i]),
                width=6*objects['a'][i],
                height=6*objects['b'][i],
                angle=objects['theta'][i] * 180. / np.pi)
    e.set_facecolor('none')
    e.set_edgecolor('red')
    ax.add_artist(e)


# available fields
objects.dtype.names    
    
flux, fluxerr, flag = sep.sum_circle(data_sub, objects['x'], objects['y'],
                                     3.0, err=bkg.globalrms, gain=1.0)

# show the first 10 objects results:
for i in range(10):
    print("object {:d}: flux = {:f} +/- {:f}".format(i, flux[i], fluxerr[i]))