import cv2
import sep
from qtpy import QtWidgets, QtCore, QtGui
import numpy as np
from Parameters import Parameters as para
import SubDialogSexConfig
from ImageData import ImageData
from ImageHandler import ImageHandler
from Ui_GalaxyWindow import Ui_GalaxyWindow

from program import cv2Qt


class SubGalaxyWindow(Ui_GalaxyWindow):
    def __init__(self):
        super(SubGalaxyWindow, self).__init__()
        self._array_e = []
        # self.setupUi(MainWindow)
        # self.actionParameters.triggered.connect(self.showSexConfigDialogWindow())

    def onButtonClick(self):
        blurred = self.myBlur()
        self.graphicsView.setPhoto(QtGui.QPixmap(blurred))

    def myBlur(self):
        img = cv2.imread('ds9.png')
        blurredImg = cv2.blur(img, (50, 50))
        blurredQImg = cv2Qt(blurredImg)
        return blurredQImg

    # def onOpenClick(self):
    #    print("onOpenClick")
    #    self.graphicsView.setPhoto(QtGui.QPixmap('dwarf.png'))

    def onCloseImage(self):
        # make sure that the source extractor items are removed
        if self._array_e != []:
            print(len(self._array_e))
            for self._eitem in self._array_e:
                self.graphicsView.scene().removeItem(self._eitem)
            self._array_e = []
        else:
            self._array_e = []

    def onSexRunClick(self):
        print("onSexRunClick")
        self._imagehandler = ImageHandler()
        self._data = self._imagehandler.origImageData
        self._bkg = sep.Background(self._data)  # extract the background with source extractor
        self._data_sub = self._data - self._bkg
        self._objects = sep.extract(self._data_sub, thresh=float(para.sex_threshold), err=self._bkg.globalrms,
                                    minarea=float(para.sex_minPixel), maskthresh=float(para.sex_maskThreshold))
        # print(len(self._objects))


        if self._array_e != []:
            print(len(self._array_e))
            for self._eitem in self._array_e:
                self.graphicsView.scene().removeItem(self._eitem)
            self._array_e = []
        else:
            self._array_e = []

        for i in range(len(self._objects)):
            self._e = QtWidgets.QGraphicsEllipseItem(self._objects['x'][i] - 3 * self._objects['a'][i],
                                                     self._objects['y'][i] - 3 * self._objects['b'][i],
                                                     6 * self._objects['a'][i], 6 * self._objects['b'][i])
            self._e.setTransformOriginPoint(self._objects['x'][i], self._objects['y'][i])
            self._e.setRotation(self._objects['theta'][i] * 180. / np.pi)
            self._e.setPen(QtGui.QPen(QtGui.QBrush(QtCore.Qt.red), 3,
                                      QtCore.Qt.SolidLine))  # QtCore.Qt.red,3,QtCore.Qt.DashDotLine, QtCore.Qt.RoundCap,QtCore.Qt.RoundJoin)#setBrush(QtGui.QBrush(QtCore.Qt.red, style = QtCore.Qt.NoBrush))
            self.graphicsView.scene().addItem(self._e)
            self._array_e.append(self._e)



    # https://pythonspot.com/pyqt5-file-dialog/
    def openFileNameDialog(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        # fileName, _ = QtWidgets.QFileDialog.getOpenFileName("QFileDialog.getOpenFileName()", "",
        #                                          "All Files (*);;Python Files (*.py)", options=options)
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName()
        if fileName:
            self.onCloseImage()
            # get image
            testData = ImageData('J024104.00-081520.0.fits')
            clippedData = testData.generateGrayscaleImage()

            qImage = QtGui.QImage(clippedData, np.size(clippedData, 0), np.size(clippedData, 1), clippedData.shape[0],
                                  QtGui.QImage.Format_Grayscale8)
            pixMap = QtGui.QPixmap.fromImage(qImage)

            self.graphicsView.setPhoto(pixMap)


    def setupActions(self):
        print("set up actions for SubGalaxyWindow")
        self.actionOpen.triggered.connect(self.openFileNameDialog)
        self.actionSexRun.triggered.connect(self.onSexRunClick)
        self.Blur.clicked.connect(self.onButtonClick)
