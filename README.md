# dependencies 
 * pip install --no-deps sep
  *(or: conda install -c openastronomy sep)
 * pip install msgpack 
 * pip install fitsio
  *(or: conda install -c openastronomy fitsio)


## qt skript
  * pyuic5 -x Ui_GalaxyWindow.ui -o Ui_GalaxyWindow.py