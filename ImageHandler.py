#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class ImageHandler:
    origImageData=None
    currentImageData=None
    def setImage(self, image_data):
        #print(image_data)
        self.__class__.origImageData = image_data

    def setCurrentImage(self, image_data):
        #print(image_data)
        self.__class__.currentImageData = image_data
