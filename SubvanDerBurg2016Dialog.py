import sep

from qtpy import QtWidgets, QtGui, QtCore
import math
from ImageHandler import ImageHandler
from Ui_vanDerBurg2016 import Ui_vanDerBurg2016Dialog
import numpy as np
from Parameters import Parameters as para


class SubvanDerBurg2016Dialog(Ui_vanDerBurg2016Dialog):
    def __init__(self):
        super(SubvanDerBurg2016Dialog, self).__init__()
        self._array_e = []


    def setupUi(self, vanDerBurg2016Dialog):
        super(SubvanDerBurg2016Dialog, self).setupUi(vanDerBurg2016Dialog)
        self.lineEdit_MinPixel.setText(str(para.sex_minPixel))
        self.lineEdit_MaxPixel.setText(str(para.sex_maxPixel))
        self.lineEdit_Threshold.setText(str(para.sex_threshold))
        self.lineEdit_Distance.setText(str(20))
        self.lineEdit_PixelScale.setText(str(0.396))
        self.lineEdit_MaxSB.setText(str(26.5))
        self.lineEdit_MinSB.setText(str(24))
        self.lineEdit_MinREff.setText(str(0.3))
        self.lineEdit_MaxREff.setText(str(7.0))
        self.setupActions()
        self.myAcceptedDetections=[]
        self.myRejectedDetections = []


    def run(self, graphicsView):
        self._mainWindowGraphicsView=graphicsView
        if self._array_e != []:
            print(len(self._array_e))
            for self._eitem in self._array_e:
                graphicsView.scene().removeItem(self._eitem)
            self._array_e = []
        else:
            self._array_e = []

        _imagehandler = ImageHandler()
        _data = _imagehandler.origImageData
        _bkg = sep.Background(_data)  # extract the background with source extractor
        # estimate background
        _data_sub = _data - _bkg

        # run initial extraction to get really everything in the image (i.e. having low constraints)
        _objects = sep.extract(_data_sub, thresh=0.7, err=_bkg.globalrms,
                               minarea=20)

        # now show all these detections in red

        # for i in range(len(_objects)):
        #    _e = QtWidgets.QGraphicsEllipseItem(_objects['x'][i] - 3 * _objects['a'][i],
        #                                             _objects['y'][i] - 3 * _objects['b'][i],
        #                                             6 * _objects['a'][i], 6 * _objects['b'][i])
        #    _e.setTransformOriginPoint(_objects['x'][i], _objects['y'][i])
        #    _e.setRotation(_objects['theta'][i] * 180. / np.pi)
        #    _e.setPen(QtGui.QPen(QtGui.QBrush(QtCore.Qt.red), 3,
        #                              QtCore.Qt.SolidLine))  # QtCore.Qt.red,3,QtCore.Qt.DashDotLine, QtCore.Qt.RoundCap,QtCore.Qt.RoundJoin)#setBrush(QtGui.QBrush(QtCore.Qt.red, style = QtCore.Qt.NoBrush))
        #    graphicsView.scene().addItem(_e)
        #    self._array_e.append(_e)

        # 1. constraint: mask compact objects. This is done by comparing the flux in the inner region (2 arc) vs the outer region
        # (7 arcsec). van der Burg+2016: A20, page 3 of 12
        _apradius = 5  # 2 arcsec TODO
        _flux2, _fluxerr2, _flag2 = sep.sum_circle(_data_sub, _objects['x'],
                                                   _objects['y'],
                                                   _apradius, err=_bkg.globalrms, gain=1.0)

        _apradius = 18  # 7 arcsec TODO
        _flux7, _fluxerr7, _flag7 = sep.sum_circle(_data_sub, _objects['x'],
                                                   _objects['y'],
                                                   _apradius, err=_bkg.globalrms, gain=1.0)
        # because we can have negative values (noise) we set these to 0, else log10 will not work
        _flux2[_flux2 < 0] = 0
        _flux7[_flux7 < 0] = 0
        # m = -2.5/ln(10) * [asinh((f/f0)/(2b)) + ln(b)].

        # convert to magnitudes
        _mag2 = 22.5 - 2.5 * np.log10(_flux2)
        _mag7 = 22.5 - 2.5 * np.log10(_flux7)

        # make a mask using the detected items and the the r2'' > 0.9 + r7'' constraint
        _mask = np.zeros(_data_sub.shape, dtype=np.bool)  # this is the mask
        for i in range(len(_objects)):
            if _mag2[i] <= 0.9 + _mag7[i]:
                sep.mask_ellipse(_mask, _objects['x'][i] - 3 * _objects['a'][i], _objects['y'][i]
                                 - 3 * _objects['b'][i], 6 * _objects['a'][i],
                                 6 * _objects['b'][i],
                                 _objects['theta'][i],
                                 r=1)

        # extract objects with parameters from dialog and masked objects
        _objects = sep.extract(_data_sub, thresh=float(para.sex_threshold), err=_bkg.globalrms,
                               minarea=float(para.sex_minPixel),
                               mask=_mask)
        # get the half-light radius of each detection
        _rhalf, _rflag = sep.flux_radius(_data_sub, _objects['x'], _objects['y'], 6. * _objects['a'], 0.5,
                                         subpix=5)
        # print(_rhalf)
        _rhalfkpc = float(self.lineEdit_Distance.text()) * 1000 * np.tan(np.deg2rad(_rhalf * 0.396 / 3600))
        # print(_rhalfkpc)
        _fluxrhalf, _fluxerrrhalf, _flagrhalf = sep.sum_circle(_data_sub, _objects['x'],
                                                               _objects['y'],
                                                               _rhalf, err=_bkg.globalrms, gain=1.0)
        _magrhalf = 22.5 - 2.5 * np.log10(_fluxrhalf)
        _magSBhalf = _magrhalf + 2.5 * np.log10(np.pi * 0.396 * 0.396 * _rhalf * _rhalf)  # TODO: px scale

        self._detections = []
        print("length of detections without constraints: " + str(len(_objects)))
        for i in range(len(_objects)):
            if float(self.lineEdit_MinSB.text()) < _magSBhalf[i] < float(self.lineEdit_MaxSB.text()) \
                    and float(self.lineEdit_MinREff.text()) < _rhalfkpc[i] < float(self.lineEdit_MaxREff.text())\
                    and _objects['npix'][i] <= float(self.lineEdit_MaxPixel.text()):
                _sexObject = SexData(_objects[i])
                self._detections.append(_sexObject)

        print("length of detections with constraints: " + str(len(self._detections)))
        self.label_ObjectsFound.setText(str(len(self._detections)))
        self.pushButton_Accept.setEnabled(True)
        self.pushButton_AcceptAll.setEnabled(True)
        self.pushButton_Reject.setEnabled(True)

        self._myindex = 0

        self.displayDetectionInDialog(self._myindex)

    def displayDetectionInDialog(self, _index):
        _snapshotScene = QtWidgets.QGraphicsScene(self.graphicsView)
        self.graphicsView.setScene(_snapshotScene)
        _photo = QtWidgets.QGraphicsPixmapItem()  # empy pixmap item
        # get snap of detection: 300x300 pixel
        _xMin = math.trunc(self._detections[_index].sexobject['x'] - 0 * self._detections[_index].sexobject['a'] - 150)
        _xMax = math.trunc(self._detections[_index].sexobject['x'] - 0 * self._detections[_index].sexobject['a'] + 150)
        _yMin = math.trunc(self._detections[_index].sexobject['y'] + 0 * self._detections[_index].sexobject['b'] - 150)
        _yMax = math.trunc(self._detections[_index].sexobject['y'] + 0 * self._detections[_index].sexobject['b'] + 150)
        _xdiff=150
        _ydiff = 150
        if _xMin < 0:
            print("_xMin small than 0")
            _xMax = _xMax + (np.abs(_xMin)+1)
            _xdiff-=  (np.abs(_xMin))
            _xMin = 0
        if _yMin < 0:
            print("_yMin small than 0")
            _yMax = _yMax + (np.abs(_yMin)+1)
            _ydiff -=  (np.abs(_yMin))
            _yMin = 0
        if _xMax > np.shape(ImageHandler().currentImageData)[1]:
            _xMin = _xMin - 1 * (np.abs(np.shape(ImageHandler().currentImageData)[1] - _xMax))
            _xMax = np.shape(ImageHandler().currentImageData)[1]
        if _yMax > np.shape(ImageHandler().currentImageData)[0]:
            _yMin = _yMin - 1 * (np.abs(np.shape(ImageHandler().currentImageData)[1] - _yMax))
            _yMax = np.shape(ImageHandler().currentImageData)[0]


        # load this snap from the currently displayed image
        _snapshotData = ImageHandler().currentImageData[_yMin:_yMax, _xMin:_xMax]
        # we need to bring the array into a C Contiguous convention, I
        # don't know what this is but this two lines make source extractor work.
        _snapshotData = np.ascontiguousarray(_snapshotData)
        _snapshotData = _snapshotData.byteswap().newbyteorder()
        # transform this snapshop data into a QImage
        _qImage = QtGui.QImage(_snapshotData, np.size(_snapshotData, 0), np.size(_snapshotData, 1),
                               _snapshotData.shape[0],
                               QtGui.QImage.Format_Grayscale8)
        # transform this QImage into a QPixmap
        _pixMap = QtGui.QPixmap.fromImage(_qImage)
        # add the QGraphicsPixmapItem to the scene
        self.graphicsView.scene().addItem(_photo)
        # add the ellipse into the scene
        self.graphicsView.scene().addItem(self._detections[_index].getEllipse(_xdiff,_ydiff))
        # set the pixelmap to the QGraphicsPixmapItem
        # _pixMap = _pixMap.scaled(_xMax-_xMin,_yMax-_yMin,QtCore.Qt.KeepAspectRatio)
        _photo.setPixmap(_pixMap)
        # make that the image is fully fitted into the view
        self.graphicsView.fitInView(QtCore.QRectF(0, 0, 300, 300), QtCore.Qt.KeepAspectRatio)


    def onAcceptAll(self):
        for i in range(len(self._detections)):
            self.acceptSingleDetection()

    def onAccept(self):
        self.acceptSingleDetection()

    def acceptSingleDetection(self):
        if self._myindex < len(self._detections) - 1:
            self.graphicsView.scene().clear()
            self.displayDetectionInDialog(self._myindex+1)
            self.displayDetectionInMainWindow(self._detections[self._myindex].sexobject)
            self.myAcceptedDetections.append(self._detections[self._myindex])
            self.pushButton_AcceptAll.setEnabled(False)
            self.label_Accepted.setText(str(len(self.myAcceptedDetections)))
            self.label_Rejected.setText(str(len(self.myRejectedDetections)))

        if self._myindex == len(self._detections) - 1:
            self.graphicsView.scene().clear()
            self.myAcceptedDetections.append(self._detections[self._myindex])
            self.updateLabelsAndButtons()

        self._myindex += 1

    def displayDetectionInMainWindow(self,_object):
        _e = QtWidgets.QGraphicsEllipseItem(_object['x'] - 3 * _object['a'],
                                            _object['y'] - 3 * _object['b'],
                                            6 * _object['a'], 6 * _object['b'])
        _e.setTransformOriginPoint(_object['x'], _object['y'])
        _e.setRotation(_object['theta'] * 180. / np.pi)
        _e.setPen(QtGui.QPen(QtGui.QBrush(QtCore.Qt.blue), 5,
                             QtCore.Qt.SolidLine))
        self._array_e.append(_e)
        # _t = QtWidgets.QGraphicsTextItem(str(int(_objects['x'][i])) + ', ' + str(int(_objects['y'][i])))
        # _t.setPos(_objects['x'][i], _objects['y'][i])
        # _t.setDefaultTextColor(QtCore.Qt.green)
        # graphicsView.scene().addItem(_t)
        self._mainWindowGraphicsView.scene().addItem(_e)

    def updateLabelsAndButtons(self):
        self.pushButton_AcceptAll.setEnabled(False)
        self.label_Accepted.setText(str(len(self.myAcceptedDetections)))
        self.label_Rejected.setText(str(len(self.myRejectedDetections)))
        self.EnableDisableButtons(False)

    def onReject(self):
        if self._myindex < len(self._detections) - 1:
            self.graphicsView.scene().clear()
            self.displayDetectionInDialog(self._myindex+1)
            self.myRejectedDetections.append(self._detections[self._myindex])
            self.pushButton_AcceptAll.setEnabled(False)
            self.label_Accepted.setText(str(len(self.myAcceptedDetections)))
            self.label_Rejected.setText(str(len(self.myRejectedDetections)))

        if self._myindex == len(self._detections)-1:
            self.graphicsView.scene().clear()
            self.myRejectedDetections.append(self._detections[self._myindex])
            self.updateLabelsAndButtons()

        self._myindex += 1


    def EnableDisableButtons(self, bool):
        """False for disable, true for enable"""
        self.pushButton_Accept.setEnabled(bool)
        self.pushButton_AcceptAll.setEnabled(bool)
        self.pushButton_Reject.setEnabled(bool)

    def setupActions(self):
        print("set up actions for SubGalaxyWindow")
        self.pushButton_Accept.clicked.connect(self.onAccept)
        self.pushButton_AcceptAll.clicked.connect(self.onAcceptAll)
        self.pushButton_Reject.clicked.connect(self.onReject)




# an instance of this class contains a detection after the van der Burg constraints are applied
class SexData(object):
    def __init__(self, sexobject):
        self.sexobject = sexobject

    def getEllipse(self,_xdiff,_ydiff):
        """Returns the ellipse of the detection."""
        _e = QtWidgets.QGraphicsEllipseItem(_xdiff - 3 * self.sexobject['a'],
                                            _ydiff - 3 * self.sexobject['b'],
                                            6 * self.sexobject['a'], 6 * self.sexobject['b'])
        _e.setTransformOriginPoint(150, 150)
        _e.setRotation(self.sexobject['theta'] * 180. / np.pi)
        _e.setPen(QtGui.QPen(QtGui.QBrush(QtCore.Qt.blue), 5,
                             QtCore.Qt.SolidLine))
        return _e
