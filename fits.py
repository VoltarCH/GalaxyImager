#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
https://stackoverflow.com/questions/12459811/how-to-embed-matplotlib-in-pyqt-for-dummies
Created on Tue Jun 12 19:11:01 2018

@author: voltar
"""
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QGraphicsScene, QGraphicsView
from PyQt5 import QtCore, QtGui, QtWidgets

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import random

class Window(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtWidgets.QPushButton('Plot')
        self.button.clicked.connect(self.plot)
        #self.plot()
        # set the layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.button)
        self.setLayout(layout)

    def plot(self):
        ''' plot some random stuff '''
        # random data
        
        from astropy.io import fits
        import matplotlib.pyplot as plt
        hdulist = fits.open('J024104.00-081520.0.fits', mode='update')
        data = hdulist[0].data
        myImage = QtGui.QImage(data, 9090, 9090, QtGui.QImage.Format_RGB32)
        #plt.imshow(scidata, cmap='gray',vmin=-0.1, vmax=0.1)
        #plt.colorbar()

        
        #data = [random.random() for i in range(10)]

        # create an axis
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.clear()

        # plot data
        #ax.plot(data, '*-')
        ax.imshow(data, cmap='gray',vmin=-0.1, vmax=0.1)
        # refresh canvas
        self.canvas.draw()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())
